# manager

Сервис для сбора информации об акциях с помощью `TINKOFF-API`, и дальнейшей её обработки.

## Организация
------------
    ├── CHANGELOG.md           <- Актуальная информация об изменениях в последних версиях.
    ├── conductor              <- Составляющая для анализа данных.
    │   │
    │   ├── models             <- Используемые обученные модели.
    │   │   │                     Наименование файла должно состоять из даты начала разработки и
    │   │   │                     названия модели. Например "2024-03-03_catboost".
    │   │   └── rejected       <- Обученные, но не используемые модели из-за их неэффективности.
    │   │                         Наименование аналогично описанному выше.
    │   │
    │   ├── notebooks          <- .ipynb notebooks. Наименование файла должно состоять из даты начала разработки
    │   │   │                     и названия модели или фичи. Например "2024-03-03_catboost".
    │   │   ├── features       <- Отчет об целесообразности добавления фичи.
    │   │   │   └── rejected   <- Отчет об нецелесообразности добавления фичи.
    │   │   └── models         <- Отчет об целесообразности добавления/изменения модели.
    │   │       └── rejected   <- Отчет об нецелесообразности добавления/изменения фичи.
    │   │
    │   └── src                <- Исходный код для использования в этой составляющей.
    │       │
    │       ├── api            <- Скрипты для обработки запросов.
    │       │
    │       ├── data           <- Скрипты для загрузки или преобразования данных.
    │       │
    │       ├── features       <- Скрипты для преобразования необработанных данных.
    │       │   │                 Наименование файла должно состоять из даты начала разработки
    │       │   │                 и названия фичи. Например "2024-03-03_add_category".
    │       │   └── rejected   <- Скрипты для преобразования необработанных данных
    │       │                     использование которых не привело к ожидаемому эффекту.
    │       │
    │       └── models         <- Скрипты для моделей. Наименование файла должно состоять из даты начала разработки
    │           │                 и названия модели. Например "2024-03-03_catboost".
    │           └── rejected   <- Модели использование которых не привело к ожидаемому улучшению.
    ├── observer               <- Составляющая для сбора данных.
    │
    ├── src                    <- Исходный код для использования в этой составляющей.
    │
    └── test                   <- Тесты.
------------
