from pathlib import Path

import psycopg2
from prettytable import from_db_cursor
from config import connect_config

queries_path = "src/sql_queries"


def read_sql(name: str, queries_path=queries_path) -> str:
    with open(Path(queries_path, name)) as f:
        return f.read()


def get_table(name_sql: str) -> str:
    with psycopg2.connect(**connect_config) as connection:
        cursor = connection.cursor()
        cursor.execute(read_sql(name_sql))
        table = from_db_cursor(cursor).get_string()

    return f"```{table}```"
