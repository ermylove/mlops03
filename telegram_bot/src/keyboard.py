from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

kb_names = {"main": ["Статистика", "Уведомления", "Управление"]}


main_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text=kb_names["main"][0]),
            KeyboardButton(text=kb_names["main"][1]),
            KeyboardButton(text=kb_names["main"][2]),
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=True,
    input_field_placeholder="Дарова",
    selective=True,
)
