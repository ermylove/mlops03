from aiogram import Bot, Dispatcher
from aiogram.filters import CommandStart
from aiogram.types import Message
import asyncio
import os

from src.keyboard import main_keyboard, kb_names
from src.helpers import get_table


bot = Bot(token=os.environ.get("TELEGRAM_TOKEN"))
dp = Dispatcher()


@dp.message(CommandStart())
async def start(message: Message):
    await message.answer("", reply_markup=main_keyboard)


@dp.message()
async def do_some(message: Message):
    kb_actions = {
        kb_names["main"][0].lower(): message.answer(
            get_table("get_bills.sql"),
            reply_markup=main_keyboard,
            parse_mode="MarkdownV2",
        ),
        kb_names["main"][1].lower(): message.answer(
            get_table("get_deals.sql"),
            reply_markup=main_keyboard,
            parse_mode="MarkdownV2",
        ),
        kb_names["main"][2].lower(): message.answer(
            get_table("get_stocks.sql"),
            reply_markup=main_keyboard,
            parse_mode="MarkdownV2",
        ),
    }

    msg = message.text.lower()
    await kb_actions.get(msg, message.answer("Wrong command"))


async def main():
    try:
        await bot.delete_webhook(drop_pending_updates=True)
        await dp.start_polling(bot)
    finally:
        await bot.session.close()


if __name__ == "__main__":
    asyncio.run(main())
