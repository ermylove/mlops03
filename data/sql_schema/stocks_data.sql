CREATE TABLE stock (
    stock_id SERIAL PRIMARY KEY,
    name_stock VARCHAR(50)
);
INSERT INTO
    stock (name_stock)
VALUES
    ('CHMF'),
    ('POLY');
