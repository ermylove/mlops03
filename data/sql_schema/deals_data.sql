CREATE TABLE deal (
    deal_id SERIAL PRIMARY KEY,
    ticker VARCHAR(12),
    price_open DECIMAL(8,4),
    price_close DECIMAL(8,4),
    close_status BOOLEAN
);
INSERT INTO
    deal (ticker, price_open, price_close, open_status)
VALUES
    ('CHMF', 11.22, 11.12, True),
    ('CHMF', 22.22, 22.42, True),
    ('POLY', 35.22, 32.22, True),
    ('POLY', 41.22, 41.62, True);
