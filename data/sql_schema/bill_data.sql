CREATE TABLE bill (
    bill_id SERIAL PRIMARY KEY,
    size_bill DECIMAL(12, 4),
    datetime TIMESTAMP,
    deal_id INT REFERENCES deal (deal_id)
);

INSERT INTO
    bill (size_bill, datetime)
VALUES
    (1100.123, '2024-04-04 23:01:00'),
    (2200.321, '2024-04-04 23:02:00'),
    (3500.144, '2024-04-04 23:03:00'),
    (4100.555, '2024-04-04 23:04:00');
